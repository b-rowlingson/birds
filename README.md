BTO Bird Survey Data
====================

This is some code to play with the
BTO Breeding and Wintering Bird Distributions in Britain and Ireland data.

The data is available here in a ZIP file under a Creative Commons license.

https://www.bto.org/our-science/data/what-data-are-available


Processing Outline
------------------

Prerequisites:

 * R
 * The `sf` package in R
 * The `sqlite3` command
 * A bash shell
 
 Process:

 * Clone this repository
 * Make a folder called `Data` and unzip the data ZIP file there. It should create some .csv files and a readme.
 * Make a folder called `Database`
 * Create the grid polygons:  `Rscript ./build_gpkg.R`
 * Fill the rest of the database: `./build_db.sh ./Database/bto.gpkg`
 
 That should create a GeoPackage with all the spatial and non-spatial data in it.
 
 
 
