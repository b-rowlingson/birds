#!/bin/sh

DB=$1

sqlite3 $DB <<EOF
.mode csv

select "species lookup";
.import 'Data/species_lookup.csv' species

select "percent benchmark species";
.import 'Data/percent_benchmark_species_detected.csv' percent_detect

select "distribution changes";
.import 'Data/distribution_changes.csv' dist_changes

select "distributions";
.import 'Data/distributions.csv' distributions

select "grid square coordinates";
.import 'Data/grid_square_coordinates_lookup.csv' grid_coords
EOF

