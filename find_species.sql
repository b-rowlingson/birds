select period, season, island, d.resolution, status, long, lat, english_name, scientific_name
 from distributions d
 join grid_coords on d.grid = grid_coords.grid 
 join species on d.speccode = species.speccode
                  where d.speccode = 258 and "order" = 1;
